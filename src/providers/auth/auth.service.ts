import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { User } from './../../models/user.model';
import { BaseService } from '../base/base.service';

@Injectable()
export class AuthService extends BaseService {

    constructor(
        public http: Http,
        public afAuth: AngularFireAuth
    ) {
        super();
    }
    createAuthUser(user: { email: string, password: string }): Promise<firebase.User> {
        return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
            .catch(this.handlePromiseError);
    }

    signinWithEmail(user: { email: string, password: string }): Promise<firebase.User> {
        return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
            .then((authUser: firebase.User) => {
                return authUser != null;
            })
            .catch(this.handlePromiseError);
    }

    logout(): Promise<void> {
        return this.afAuth.auth.signOut();
    }

    get authenticated(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.afAuth
                .authState
                .first()
                .subscribe((authUser: firebase.User) => {
                    (authUser) ? resolve(true) : reject(false);
                });
        });
    }
}