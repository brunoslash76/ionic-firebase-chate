import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { BaseService } from "../base/base.service";
import { Chat } from "../../models/chat.model";
import { AngularFireObject } from 'angularfire2/database/interfaces';

@Injectable()
export class ChatService extends BaseService {
    constructor(
        public afAuth: AngularFireAuth,
        public db: AngularFireDatabase,
        public http: Http
    ){
        super();
    }

    create(chat: Chat, userId1: string, userId2: string): Promise<void>{
        return this.db.object<Chat>(`/chats/${userId1}/${userId2}`)
        .set(chat)
        .catch(this.handlePromiseError);
    }

    getDeepChat(userId1: string, userId2: string): AngularFireObject<Chat> {
        return this.db.object<Chat>(`/chats/${userId1}/${userId2}`);
    }
}