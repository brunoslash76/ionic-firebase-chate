import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AuthService } from './../../providers/auth/auth.service';
import { Chat } from './../../models/chat.model';
import { ChatPage } from './../chat/chat';  
import { ChatService } from './../../providers/chat/chat.service';
import { SignupPage } from './../signup/signup';
import { User } from './../../models/user.model';
import { UserService } from './../../providers/user/user.service';

import * as firebase from 'firebase/app';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  users: Observable<User[]>;
  view: string = 'chats';

  constructor(
    public authService: AuthService,
    public chatService: ChatService,
    public navCtrl: NavController,
    public userService: UserService
  ) { }

  ionViewCanEnter(): Promise<boolean> {
    return this.authService.authenticated;
  }

  ionViewDidLoad() {
    this.users = this.userService.users;
  }

  onChatCreate(recipientUser: User): void {

    this.userService
    .mapObjectKey<User>(this.userService.currentUser)
    .first()
    .subscribe((currentUser: User) => {
      this.chatService
        .mapObjectKey<Chat>(this.chatService.getDeepChat(currentUser.$key, recipientUser.$key))
        .first()
        .subscribe((chat: Chat) => {
          if(!chat.title){
            let timestamp: Object = firebase.database.ServerValue.TIMESTAMP;
            let chat1 = new Chat('', timestamp, recipientUser.name, (recipientUser.photo || ''));
            this.chatService.create(chat1, currentUser.$key, recipientUser.$key);
            let chat2 = new Chat(``, timestamp, currentUser.name, (currentUser.photo || ''));
            this.chatService.create(chat2, recipientUser.$key, currentUser.$key)
          }
        });
    })

    this.navCtrl.push(ChatPage, {
      recipientUser: recipientUser
    })

  }

  signup(): void {
    this.navCtrl.push(SignupPage);
  }

}
