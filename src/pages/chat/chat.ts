import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthService } from './../../providers/auth/auth.service';
import { Chat } from '../../models/chat.model';
import { User } from './../../models/user.model';
import { UserService } from '../../providers/user/user.service';
import { AngularFireObject } from 'angularfire2/database/interfaces';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  messages: string[] = [];
  pageTitle: string;
  sender: User;
  recipient: User;
  private chat1: AngularFireObject<Chat>;
  private chat2: AngularFireObject<Chat>;


  constructor(
    public userService: UserService,
    public authService: AuthService,
    public navCtrl: NavController, 
    public navParams: NavParams
  ) {
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.authService.authenticated;
  }

  ionViewDidLoad() {
    this.recipient = this.navParams.get('recipientUser');
    this.pageTitle = this.recipient.name;
    this.userService
    .mapObjectKey<User>(this.chat1)
    .first()
    .subscribe((currentUser: User) => {
      this.chatService.updatePhoto(this.chat1, chat.photo, this.recipient.photo)
    })

  }

  sendMessage(newMessage: string): void {
    this.messages.push(newMessage);
  }

}
