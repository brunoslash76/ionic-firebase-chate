import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'capitalize'
})
@Injectable()
export class CapitalizePipe implements PipeTransform {
    transform(value: string, onlyFirst: boolean) {

        if(onlyFirst) return value.charAt(0).toUpperCase() + value.substring(1);

        let word: string[] = value.split(' ');
        let output: string = '';

        word.forEach( (value: string, index: number, words: string[]) => {
            output += value.charAt(0).toUpperCase() + value.substring(1).toLocaleLowerCase() + ' ';
        }); 
        return output;
    } 
}