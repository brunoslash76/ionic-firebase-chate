import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule, FirebaseAppConfig, } from 'angularfire2';
import AuthMethods from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AuthService } from './../providers/auth/auth.service';
import { CapitalizePipe } from './../pipes/capitalize.pipe';
import { ChatPage } from './../pages/chat/chat';
import { ChatService } from '../providers/chat/chat.service';

// import { MessageBoxComponent } from './../components/message-box/message-box.component';
// import { MessageService } from './../providers/message.service';
import { HomePage } from '../pages/home/home';
import { MyApp } from './app.component';
// import { ProgressBarComponent } from './../components/progress-bar/progress-bar.component';

import { SigninPage } from './../pages/signin/signin';
import { SignupPage } from './../pages/signup/signup';
// import { UserInfoComponent } from './../components/user-info/user-info.component';
// import { UserMenuComponent } from './../components/user-menu/user-menu.component';
// import { UserProfilePage } from './../pages/user-profile/user-profile';
import { UserService } from './../providers/user/user.service';
import { CustomLoggedHeaderComponent } from '../components/custom-logged-header/custom-logged-header';

const firebaseAppConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyDJouBvjFwYgC27rl7fwGkkknGboKfZ5G4",
  authDomain: "fir-chat-36807.firebaseapp.com",
  databaseURL: "https://fir-chat-36807.firebaseio.com",
  storageBucket: "fir-chat-36807.appspot.com",
  messagingSenderId: "1072254125484"

};

@NgModule({
  declarations: [
    CapitalizePipe,
    ChatPage,
    CustomLoggedHeaderComponent,
    HomePage,
    // MessageBoxComponent,
    MyApp,
    // ProgressBarComponent,
    SigninPage,
    SignupPage,
    // UserInfoComponent,
    // UserMenuComponent,
    // UserProfilePage
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseAppConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ChatPage,
    HomePage,
    MyApp,
    SigninPage,
    SignupPage,
    // UserProfilePage
  ],
  providers: [
    AuthService,
    ChatService,
    // MessageService,
    StatusBar,
    SplashScreen,
    UserService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }